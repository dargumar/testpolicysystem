package com.policy;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

import junit.framework.Assert;

public class PolicyObjectTest {
	Policy policy;

	@Before
    public void setup(){
		String requestString = "{\"number\":\"LV19-07-100000-1\",\"status\":\"REGISTERED\",\"objects\":[{\"name\":\"a flat\",\"objects\":[{\"name\":\"TV\",\"sum\":\"500\",\"riskType\":\"FIRE\"},{\"name\":\"PC\",\"sum\":\"100\",\"riskType\":\"WATER\"}]}]}";
		policy = new Gson().fromJson(requestString, Policy.class);
    }

	@Test
	public void exampleTest1() {
		Assert.assertEquals(PremiumCalculator.calculate(policy).getPremium(), 16.5);
	}

    @Before
    public void setup2(){
		String requestString = "{\"number\":\"LV19-07-100000-1\",\"status\":\"REGISTERED\",\"objects\":[{\"name\":\"a flat\",\"objects\":[{\"name\":\"TV\",\"sum\":\"100\",\"riskType\":\"FIRE\"},{\"name\":\"PC\",\"sum\":\"8\",\"riskType\":\"WATER\"}]}]}";
		policy = new Gson().fromJson(requestString, Policy.class);
    }

    @Test
	public void exampleTest2() {
		Assert.assertEquals(PremiumCalculator.calculate(policy).getPremium(), 2.1);
	}

}
