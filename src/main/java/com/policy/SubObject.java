package com.policy;

import com.google.gson.annotations.SerializedName;

public class SubObject {
	@SerializedName("name")
	private String name;

	@SerializedName("sum")
	private double sum;

	@SerializedName("riskType")
	private String riskType;

    public double getSum() {
		return sum;
	}

	public String getType() {
		return riskType;
	}
}
