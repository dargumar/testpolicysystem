package com.policy;

import com.google.gson.annotations.SerializedName;


public class PolicyObject {
    private final static double coefFireDefault = 0.013;
    private final static double coefFire = 0.023;
    private final static double coefWaterDefault = 0.1;
    private final static double coefWater = 0.05;

	@SerializedName("name")
	private String name;

	@SerializedName("objects")
	private SubObject[] objects;

	public double getPolicySum(String typeString) {
    	double policySum = 0;
    	for (SubObject so : this.objects)
    		if(so.getType().toUpperCase().equals(typeString))
    			policySum += so.getSum();
    	
    	return policySum * getInterestCoefficient(policySum, typeString);
    }

	private double getInterestCoefficient(double sum, String typeString) {
		switch(typeString) {
			case "WATER":
				if(sum <= 10)
					return coefWaterDefault;
				return coefWater;
			case "FIRE":
				if(sum <= 100)
					return coefFireDefault;
				return coefFire;
		}
		 return coefWater;
	}
}
