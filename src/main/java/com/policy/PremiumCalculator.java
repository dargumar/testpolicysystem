package com.policy;

public class PremiumCalculator {

	public static Policy calculate(Policy policy) {
		double policySum = 0;

		for (PolicyObject po : policy.getPolicyObjects()) {
			policySum += po.getPolicySum("FIRE");
			policySum += po.getPolicySum("WATER");
		}
		policy.setPremium(policySum);
		return policy;
	}
}
