package com.policy;

import static spark.Spark.*;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class RouteHandler extends Common{
    public static void main(String[] args) {
    	path("/api", () -> {
    	    before("/*", (q, a) -> System.out.println("Received api call"));
    	    path("/policy", () -> {
    	    	post("/calculate", (request, response) -> {
    	    		Policy policy = new Gson().fromJson(request.body(), Policy.class);
        	    	policy = PremiumCalculator.calculate(policy);
        	    	if(policy.getPremium() != 0) {
        	    		apiResponse(response, HttpServletResponse.SC_OK);
        	    	} else {
            	    	apiResponse(response, HttpServletResponse.SC_BAD_REQUEST);
        	    	}
        	    	ResponseObject t = new ResponseObject(policy);
    	    		return new Gson().toJson(t);
        	    });
    	    });
    	});
    }
}

