package com.policy;

import com.google.gson.annotations.SerializedName;

public class Policy {
    @SerializedName("number")
	private String policyNumber;

    @SerializedName("status")
	private String policyStatus;

    @SerializedName("objects")
    private PolicyObject[] policyObjects;

    @SerializedName("premium")
    private double premium;

    public String getPolicyNumber() {
		return policyNumber;
	}

    public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

    public String getPolicyStatus() {
		return policyStatus;
	}

    public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

	public PolicyObject[] getPolicyObjects(){
		return this.policyObjects;
	}

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}
}
