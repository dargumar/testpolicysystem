package com.policy;

import com.google.gson.annotations.SerializedName;

public class ResponseObject {
	@SerializedName("data")
	private Policy data;

	public ResponseObject(Policy policy) {
		super();
		this.data = policy;
	}	
}
