package com.policy;

import spark.Response;

public abstract class Common {
	public static Response apiResponse(Response response, int status) {
		response.type("application/json");
		response.status(status);
		return response;
	}
}
